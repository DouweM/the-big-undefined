## Methodology

The following is run on a hourly GitLab CI schedule:

1. This downloads the teams definition from the [async-retrospectives] and from [this repository][teams.yml] and joins the two.
2. Get every `backend`, `frontend` userID and `group` member if a group is defined
3. Get the _merged_ Merge Requests each user created after 2019-04-01
4. If they are missing the appropriate Stage label (e.g. `~Secure`, `~Plan`), report the Merge Request in this report


## My Stage is missing. How do I add it to this project?

If you are not using [async-retrospectives], please create an MR that adds your stage to the [teams.yml].
If you want to overwrite the settings for your stage, you can it in the [teams.yml] as well.

[async-retrospectives]: https://gitlab.com/gitlab-org/async-retrospectives/blob/master/teams.yml
[teams.yml]: https://gitlab.com/leipert-projects/the-big-undefined/blob/master/teams.yml
