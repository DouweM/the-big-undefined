const fs = require("fs");
const path = require("path");
const pug = require("pug");
const md = require("markdown-it")();

const PAGE_TITLE = "Merge Requests without proper stage label";

const compiledStageTemplate = pug.compileFile(
  path.join(__dirname, "../templates/stage.pug")
);

function printStage(info) {
  const renderedPage = compiledStageTemplate({
    ...info,
    title: `${info.stageName}: ${PAGE_TITLE}`
  });

  fs.writeFileSync(`./public/${info.stageSlug}.html`, renderedPage);
}

function printIndexPage(stages) {
  const methodology = md.render(
    fs.readFileSync(path.join(__dirname, "../README.md"), "utf-8")
  );

  const renderedPage = pug.renderFile(
    path.join(__dirname, "../templates/index.pug"),
    {
      ...stages,
      title: PAGE_TITLE,
      methodology
    }
  );

  fs.writeFileSync(path.join(__dirname, "../public/index.html"), renderedPage);
}

module.exports = {
  printStage,
  printIndexPage
};
