const fs = require("fs");

const yamlParser = require("js-yaml");

const { getStageInfo } = require("./lib/api");
const { printStage, printIndexPage } = require("./lib/rendering");

async function renderStages(allStages) {
  const stages = Object.entries(allStages)
    .map(([stageName, value]) => {
      const { label, backend = [], frontend = [], group = [] } = value;

      return {
        stageName,
        stageLabel: label,
        stageSlug: stageName
          .toLowerCase()
          .replace(/\W/g, "-")
          .replace(/-+/g, "-"),
        group,
        users: [...backend, ...frontend]
      };
    })
    .sort((a, b) => (a.stageName > b.stageName ? 1 : -1));

  const stageInformation = await Promise.all(stages.map(getStageInfo));

  printIndexPage({ stages });
  stageInformation.forEach(printStage);
}

// Get document, or throw exception on error
const asyncRetroStages = yamlParser.safeLoad(
  fs.readFileSync("./async-teams.yml", "utf8")
);
const localStages = yamlParser.safeLoad(fs.readFileSync("./teams.yml", "utf8"));

const allStages = process.env.CI
  ? { ...asyncRetroStages, ...localStages }
  : { Secure: asyncRetroStages.Secure };

renderStages(allStages)
  .then(() => process.exit(0))
  .catch(e => {
    console.log(e);
    return process.exit(1);
  });
