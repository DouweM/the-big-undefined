#!/usr/bin/env bash
# Use the unofficial bash strict mode: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail; IFS=$'\n\t'

curl --fail --location https://gitlab.com/gitlab-org/async-retrospectives/raw/master/teams.yml --output async-teams.yml
mkdir --parents public/
node ./index.js
